console.log("Hello World");

const a = 10;
const b = 2;
const c = a + b;
const d = a - b;
const e = a * b;
const f = a / b;
const g = a % b;
const h = a ** b;
const i = b % a;
const j = b ** a;
const k = (a + b) / 2;
console.log(c, d, e, f, g, h, i, j, k);

const l = 3;
const m = 12;
if (l > m) {
    console.log("l is larger than m!");
} else if (m > l) {
    console.log("m is larger than l!");
} else {
    console.log("they must be equal!");
}

let val; //ei annettu arvoa eli undefined eli falsy//
if (val) {
    console.log("val is truthy!");
} else {
    console.log("val is falsy!");
}

const age = 19;
const canDrive = age > 16 ? "yes" : "no";
console.log(canDrive);

let lvl; //yritin constilla mutta sitä ei saa jättää arvotta joten let //
const areYouPro = lvl >= 9000 ? "Awesome!" : "Keep practising!";
console.log(areYouPro);

const fruit = "banana";
switch (fruit) {
    case "banana":
        console.log("the fruit is banana");
        break;
    case "apple":
        console.log("the fruit is apple");
        break;
}
let hasTomato = true;
let hasCheese = false;
if (hasTomato && hasCheese) {
    console.log("Is pizza");
} else {
    console.log("Is not pizza");
}

//Exercise 1:if conditionals, dia 47//

const playerCount = 3;
if (playerCount >= 4) {
    console.log("You can play!");
} else {
    console.log("Sorry, you can not play");
}

const isStressed = false;
const hasIcecream = false;
if (isStressed !== true || hasIcecream) {
    console.log("Mark is happy");
} else {
    console.log("Mark is sad");
}

const sunny = true;
const rainy = false;
const temp = 20;
if (sunny && rainy !== true && temp >= 20) {
    console.log("It is a beach day!");
} else {
    console.log("It is not a beach day :(");
}

const seeSuzy = false;
const seeDan = true;
const day = "Tuesday";
if (seeSuzy && seeDan !== true && day === "Tuesday" || // Tässä olettaa että seeSuzy === true
    seeSuzy !== true && seeDan && day === "Tuesday") {
    console.log("Arin is happy");
} else {
    console.log("Arin is mad");
}

//Exercise 2: leap year, dia 48 //

const year = 2000;
if (year % 4 === 0 && year % 100 !== 0 || year % 400 === 0) {
    /*joka 4.vuosi, paitsi tasavuosisadat vain jos jaollisia
    400lla */
    console.log("It is a leap year");
} else {
    console.log("It is not a leap year");
}

//Strings//

const myName = " Matti ";
console.log(myName.length);
console.log(myName.trim());
console.log(myName);
const genre = "dub" + "step";
console.log(genre);
const nameSub = "villegalle";
console.log(nameSub.substr(1, 7));
console.log(nameSub.substr(5));
const firstname = "Maija";
const surname = "Mehiläinen";
const desciption = "User is " + age + " years old. Their name is "
    + firstname + " " + surname + ".";
console.log(desciption);
const secondDesc = `User is ${age} years old. Their name is ${firstname} ${surname}.`; //${} kanssa ``
console.log(secondDesc);

//Exercise 3, dia 55 //

const str1 = "Some";
const str2 = "thing";
const str_sum = str1 + str2;
console.log(str_sum);
console.log(str1.length, str2.length, str_sum.length);
const str_avg = (str1.length + str2.length) / 2;
console.log(str_avg);
if (str1.length < str_avg) {
    console.log(str1);
} else {
    console.log("-");
}
if (str2.length < str_avg) {
    console.log(str2);
} else {
    console.log("-");
}
if (str_sum.length < str_avg) {
    console.log(str_sum);
} else {
    console.log("-");
}

const someArray = ["thing1", "thing2", 34.4, true, undefined];
const someOtherArray = [4, 1, 5, 234];
someArray[0] = "newthing1"; //laskeminen alkaa 0sta//
console.log(someArray);
console.log(someOtherArray.length);
console.log(someArray[0]);
console.log(someArray[someArray.length - 2]);

// Array: exercise 1, dia 77 //

const arr = ["banaani", "omena", "mandariini", "appelsiini", "kurkku", "tomaatti", "peruna"];
arr.sort(); //hox! sipuli tulee viimeiseksi, koska push on sortin jälkeen//
const filteredArr = arr.filter((arr) => {
    return arr.includes("r");
});
console.log(filteredArr);  // methodeissa vasta, ~40 diaa eteenpäin x'D//
arr.shift(); //shift poistaa alusta, pop lopusta eli appelsiini poistuu koska sort ennen tätä //
arr.push("sipuli"); //push tulee loppuun, unshift alkuun//
console.log(arr);

// Loops //

let sum = 0;
for (let ind = 0; ind <= 100; ind++) {
    sum = sum + ind;
}
console.log(sum);

let ind = 0;
let summa = 0;

while (ind <= 100) {
    summa = summa + ind;
    ind = ind + 1;
}
console.log(summa);

// Loops: Exercise 2, dia 82 //

const myArr1 = [];
for (let i = 0; i < 1001; i += 100) { //i täytyy olla let koska arvo muuttuu kokoajan// 
    myArr1.push(i);
}
console.log(myArr1);

const myArr2 = [];
for (let i = 1; i < 129; i += i) {
    myArr2.push(i);
}
console.log(myArr2);

const myArr3 = [];
for (let i = 3; i < 16; i += 3) {
    myArr3.push(i);
}
console.log(myArr3);

const myArr4 = [];
for (let i = 9; i > -1; i--) {
    myArr4.push(i);
}
console.log(myArr4);

/* 111222333444
Ei mitään hajua 
const myArr5 = [];
for (let i = 0; i < 5; i++) {
    myArr5.push(i);
    myArr5.repeat(3);
}
console.log(myArr5); */

const myArr6 = [];
for (let i = 0; i < 5; i++) {
    myArr6.push(i);
} for (let i = 0; i < 5; i++) {
    myArr6.push(i);
} for (let i = 0; i < 5; i++) {
    myArr6.push(i);
}
console.log(myArr6); // Uglyyyyyy!//

//Array Methods
const instruments = ["Accordion", "Piano", "Synth", "Nyckelharpa"];

/*forEach loops through all the elements of the array
function is taken as an arument which takes in every element
of the array*/
instruments.forEach(function (instr) {
    console.log(instr);
});
//tai nuolellla
instruments.forEach((instr) => {
    console.log(instr);
});

/* Map function creates a new array from the old one*/
const lengths = instruments.map(function (instr) {
    return instr.length;
});
console.log(lengths);
//tai nuolella
const lenghtsNuoli = instruments.map((instr) => instr.length);
console.log(lenghtsNuoli);
//sama forloopilla
const lengthsLoop = [];
for (let i = 0; i < instruments.length; i++) {
    lengthsLoop[i] = instruments[i].length;
}
console.log(lengthsLoop);

/*Filter can pick elements from an array to a new, smaller array
function returns the condition on which the element gets picked*/
const longintruments = instruments.filter(function (instr) {
    return instr.length > 6;
});
console.log(longintruments);
// nuolella
const longintrumentsNuoli = instruments.filter((instr) => instr.length > 6);
console.log(longintrumentsNuoli);

/*Find returns the first array element for which the condition is true
Argument is a funtion that returns a condition */
const foundPiano = instruments.find(function (instr) {
    return instr === "Piano";
});
console.log(foundPiano);
//nuolella
const foundInstr = instruments.find((instr) => instr === "Piano"); // !!! Tarkka oikeinkirjoituksesta!! Isot kirjaimet !!
console.log(foundInstr);

/* Reduce turns an array into a new singe item. This new single object
can be new array, new object or number/string
Doesn't produce a new array but rather one Parameter that is
calculated by using all the array elements */
const lenghtsSum = lengths.reduce(function (accumulator, element) {
    return accumulator + element;
}, 0);
console.log(lenghtsSum);
//nuolella
const sumNuoli = lengths.reduce((accumulator, element) => accumulator + element, 0);
console.log(sumNuoli);

/*Different ways to use array methods
// with an inline function expression
const lengths = instruments.map(function(instr) {return instr.length});
// or more concisely with the arrow notation
const lengths = instruments.map((instr) => instr.length);
// or by using an earlier function declaration
function mapFunc(instr) {
    return instr.length;
}
const lengths = instruments.map(mapFunc);*/

//Dia 125: Exercise 4: Array manipulation
const arrMet = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22];
/*
From the elements of this array,
1. create a new array with only the numbers that are divisible by three.
2. Create a new array from original array (arr), where each number is multiplied by 2
3. Sum all of the values in the array using the array method reduce

console.log the result after each step */

const divByThree = arrMet.filter((arrMet) => arrMet % 3 === 0);
console.log(divByThree);
/*
arr.forEach((arr) => { // Ei siis tee uutta arrayta
    console.log(arr * 2);
});*/

const doubled = arrMet.map((arrMet) => arrMet * 2);
console.log(doubled);

const total = arrMet.reduce((currentTotal, arrMet) => {
    return arrMet + currentTotal;
}, 0);
console.log(total);

const doubledTotal = doubled.reduce((currentTotal, doubled) => {
    return doubled + currentTotal;
}, 0);
console.log(doubledTotal);

//Dia 127: Exercise 6: Merge arrays
/* Create a program that joins two arrays together. If the same value appears in both arrays, 
the function should return that value only once.

[1, 2, 3, 4]
[3, 4, 5, 6]
return [1, 2, 3, 4, 5, 6]

Use different ways to join the arrays: Get to know Array.concat() method and spread syntax */

let arr1 = [1, 2, 3, 4];
let arr2 = [3, 4, 5, 6];

// arr1 = arr1.concat(arr2); perus concat
// arr1 = [...arr1, ...arr2]; perus spread syntax

let arr3 = arr1.concat(arr2.filter((item) => arr1.indexOf(item) < 0)); //filter out the unique items in arr2 before concatenating with arr1
console.log(arr3);