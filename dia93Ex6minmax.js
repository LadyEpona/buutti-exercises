const array = [1, 4, 6, 32, 25, 16, 31, 15, 10, 2, 7];

/*Print out the largest number found in this array.

Find the largest number without first sorting the array, or without using Math.max() function.*/

const forLoopMinMax = () => { //Eli tässä tehdään funktio, jonka sisällä forloop
    let min = array[0], max = array[0];

    for (let i = 1; i < array.length; i++) {
        let value = array[i];
        min = (value < min) ? value : min; //Mitäs ? merkkaa o.O Kolmiehto!
        max = (value > max) ? value : max;
    }

    return [min, max];
};
const [forLoopMin, forLoopMax] = forLoopMinMax();
console.log(`Minimum: ${forLoopMin}, Maximum: ${forLoopMax}`);

/*Level 2:
Find the 2nd largest number in the array, without sorting the array first, or using Math functions to aid you.*/