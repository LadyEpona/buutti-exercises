/* Create a program that generates 7 random numbers from 1 to 40. Numbers must be unique.

Return array of lottery numbers.

Extra: output each number with one second delay
Hint for extra: get to know setTimeout() */

function generateLottoNum() {
    let lottoNum = Math.floor(Math.random() * 50) + 1; /* max on 50, +1 siksi että floor pyöristää alaspäin
    eli näin ottaa myös maxnumeron */
    return lottoNum;
}

function winningNums() {
    let winningNumsArray = []; //temp array

    while (winningNumsArray.length < 6) {
        let num = generateLottoNum();

        if (winningNumsArray.includes(num) === false) { //jos numero ei ole vielä arrayssa niin lisää sen
            winningNumsArray.push(num);
        }
    }

    return [...winningNumsArray].sort(orderingNums); // .sortista eteenpäin jos ottaa pois niin numerot eivät tule suuruusjärjestyksessä
}

function orderingNums(a, b) { /*en ymmärrä miten tää toimii o.O */
    return a - b;
}

console.log(winningNums());