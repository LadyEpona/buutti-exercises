const readline = require("readline");

const rl = readline.createInterface(
    process.stdin, process.stdout);

rl.setPrompt("Teksti tähän: ");
rl.prompt();
rl.on("line", (teksti) => {
    teksti = teksti.trim();
    teksti = teksti.toLowerCase();
    console.log(teksti.substr(0, 20));
    rl.close();
});

