import readline from "readline-sync";

const forecast = [
    {
        "Temperature": "20 celsius degree",
        "Cloudy": "yes",
        "Sunny": "no",
        "Wind": "yes"
    },
    {
        "Temperature": "5 celsius degree",
        "Cloudy": "yes",
        "Sunny": "no",
        "Wind": "yes"
    },
    {
        "Temperature": "28 celsius degree",
        "Cloudy": "no",
        "Sunny": "yes",
        "Wind": "no"
    }
]

console.log("Hi! I am a dumb chat bot.")
console.log("You can check all the things I can do by typing 'help'.")
const answer = readline.question();
if (answer === "help" || answer === "Help") {
    console.log("-----------------------------");
    console.log("Here´s a list of commands that I can execute!");
    console.log("help: Opens this dialog.");
    console.log("hello: I will say hello to you");
    console.log("botInfo: I will introduce myself");
    console.log("botName: I will tell my name");
    console.log("botRename: You can rename me");
    console.log("forecast: I will forecast tomorrows weather 100% accurately");
    console.log("quit: Quits the program.");
    console.log("-----------------------------");
} else {
    console.log("Error, please try again"); //Nyt vain jatkaa eteenpäin...
}
const secondAnswer = readline.question();
if (secondAnswer === "hello" || secondAnswer === "Hello") {
    console.log("What is your name?");
    const name = readline.question();
    console.log(`Hello there, ${name}`);
} else if (secondAnswer === "botInfo" || secondAnswer === "BotInfo" || secondAnswer === "botinfo") {
    let n = 2; // Milläs tän nyt sit saa laskemaan ^^'
    console.log(`I am a dumb chat bot. You can ask me many things :). You have already asked me ${n} questions.`);
} else if (secondAnswer === "botName" || secondAnswer === "BotName" || secondAnswer === "botname") {
    let defName = "Henry";
    console.log(`My name is currently ${defName}. If you want to change it type 'botRename'.`);
} else if (secondAnswer === "botRename" || secondAnswer === "BotRename" || secondAnswer === "botrename") {
    console.log("Type my new name, please.");
    let newBotName = readline.question();
    console.log(`Are you happy with the name ${newBotName}?`);
    let previousName = "Henry";
    let renamingOk = readline.question();
    if (renamingOk === "yes" || renamingOk === "Yes") {
        console.log(`I was renamed to ${newBotName}`);
    } else {
        console.log(`Name not changed. My name is still ${previousName}.`)
    }
} else if (secondAnswer === "forecast" || secondAnswer === "Forecast") {
    console.log("Tomorrows weather will be...")
    const randomForecast = Math.floor(Math.random() * forecast.length);
    console.log(forecast[randomForecast]);
} else if (secondAnswer === "quit" || secondAnswer === "Quit") {

}
