const students = [
    { name: "markku", score: 99 }, { name: "karoliina", score: 58 },
    { name: "susanna", score: 69 }, { name: "benjamin", score: 77 },
    { name: "isak", score: 49 }, { name: "liisa", score: 89 },
];
/*Find the highest, lowest scoring person.
Then find the average score of the students.
Then print out only the students who scored higher than the average. */

const maxScore = students.reduce(function (prev, current) {
    return (prev.score > current.score) ? prev : current; // Ternary! :D condition ? expression 1: expression 2; eli niinkuin if/else
});
console.log(maxScore);

const minScore = students.reduce(function (prev, current) {
    return (prev.score < current.score) ? prev : current;
});
console.log(minScore);

const totalScore = students.reduce((currentTotalScore, students) => {
    return students.score + currentTotalScore;
}, 0);
const studentsScore = students.map((students) => {
    return students.score;
});
const averageScore = (totalScore / studentsScore.length);
console.log(averageScore);

const aboveAveStudents = students.filter((students) => {
    return students.score > averageScore;
});
console.log(aboveAveStudents);

/* Assign grades 1-5 for each student based on their scores like this:
    "1": "1-39",
    "2": "40-59",
    "3": "60-79",
    "4": "80-94",
    "5": "95-100"

students.forEach((students) => {
    if (students.score >= 95) {
        students.push(1); // Ei tällä, concat? Ei sekään...
    } else
    }) */