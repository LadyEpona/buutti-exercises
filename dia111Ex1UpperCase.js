const sentence = "super cool morning and hello world!";

function capitalize(sentence) {
    return sentence.charAt(0).toUpperCase() + sentence.slice(1); //täällä ei viä ymmärrä mitä numerot merkkaa
}
const caps = sentence.split(" ").map(capitalize).join(" ");
console.log(caps); /*Splitillä alkuperäinen string eli sentence erillisiksi stringeiksi eli sanoiksi välin kohdalla, siksi .split("välitässä")
ja liitetään takaisin yhdeksi stingiksi joinilla ja halutaan välit takaisin, siksi .join("välitässä") */