/* Modify the previous program so that only the multiples of three or five are considered in the sum (Hint: The Modulus operator will help you with this)

e.g. if n=17, we’d consider 3, 5, 6, 9, 10, 12 and 15, and sum= 60 */

const readline = require("readline");

const rl = readline.createInterface(
    process.stdin, process.stdout);

rl.setPrompt("Give a number, please: ");
rl.prompt();
rl.on("line", (number) => {
    let integers = 0;
    for (let ind = 0; ind <= (number); ind++)
        if (ind % 3 === 0 || ind % 5 === 0) { //Pitkän aikaa ehtona oli integers, huuuuups
            integers = integers + ind;
        }  /*else if (ind % 5 === 0) { Tämän saikin heitettyä suoraan tuohon ylemmäs
            integers = integers + ind;
        }*/ else {
            ind = ind++;
        }
    console.log(`The sum of integers up to this number is ${integers}`);
    rl.close();
});
//Ei kyllä nyt mitään hajua mitä tää laskee !! En oo määrittäny et muita ei lasketa !! //