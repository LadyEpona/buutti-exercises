import fs from "fs";

//Ei kannata käyttää koska vie paljon muistia
/*fs.writeFile("./writeFile.txt", "Example text", (err) => {
    if (err) console.log(err);
    else console.log("success");
})
fs.readFile("./writeFile.txt", "utf-8", (err, file) => {
    if (err) console.log(err);
    else console.log(file);
})*/

// Streamaus on parempi
/*const stream = fs.createWriteStream("./writeStream.txt");
stream.write("Hello, I am stream!", (err) => {
    if (err) console.log(err);
    else console.log("success");
});
const readStream = fs.createReadStream("./writeStream.txt", "utf-8");
readStream.on("data", (txt) => {
    console.log(txt);
});*/

/* const readStream = fs.createReadStream("./textFile.txt", "utf-8");
readStream.on("data", (txt) => {
    const regex = /joulu/ig;
    console.log(txt.replaceAll(regex, "kinkku")); // Toimii, mutta jää pieneksi vaikka lauseen alussa
});*/

/*const readStream = fs.createReadStream("./textFile.txt", "utf-8");
readStream.on("data", (txt) => {
    const txt2 = txt.replaceAll("joulu", "kinkku"); // No upeaa koodia
    const txt3 = txt2.replaceAll("Joulu", "Kinkku"); // Toimii kyllä
    const txt4 = txt3.replaceAll("lapsilla", "poroilla");
    const txt5 = txt4.replaceAll("Lapsilla", "Poroilla");
    console.log(txt5);
});*/

const readStream = fs.createReadStream("./textFile.txt", "utf-8");
readStream.on("data", (txt) => { // Onko tää ny parempi ku edellinen? xD
    //  console.log(txt.replaceAll("joulu", "kinkku").replaceAll("Joulu", "Kinkku").replaceAll("lapsilla", "poroilla").replaceAll("Lapsilla", "Poroilla"));
    const stream = fs.createWriteStream("./textFile.txt");
    stream.write(txt.replaceAll("joulu", "kinkku").replaceAll("Joulu", "Kinkku").replaceAll("lapsilla", "poroilla").replaceAll("Lapsilla", "Poroilla"), (err) => {
        if (err) console.log(err);
        else console.log("success");
    });
});

/*
fs.writeFile("./textFile.txt", txt5, (err) => { //Niiii ei se hae tuolta sisältä...
    if (err) throw err;
    console.log('The file has been saved!');
});*/