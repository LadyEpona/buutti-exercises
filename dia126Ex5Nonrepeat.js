/*Find the first non-repeating character from a string.

For example, in “aabbooooofffkkccjdddTTT” it would be “j”.

Hint: use array method split to transform the string into array.
*/

/*const randomString = "aabbooooofffkkccjdddTTT";
const stringSeparated = randomString.split("");

const nonRepeated = stringSeparated.filter((str) => {
    for (let i = 0; i < str.length; i++) {
        let j = str.charAt(i);
        if (str.indexOf(j) === str.lastIndexOf(j)) {
            return j;
        }
    }
    return null;
});

console.log(nonRepeated); */

/*const firstNonRepeating = stringSeparated.reduce(function (prev, current) {
    return (prev === current) ? prev : current;
});*/
//const firstNonRepeating = 
/*
const firstNonRepeating = stringSeparated.find((current, prev) => {
    return (prev === current) ? prev : current;
});

console.log(firstNonRepeating);*/
/*const firstNonRepeating = function (string) {
    for (let i = 0; i < string.lenght; i++) {
        if (stringSeparated.filter(function (j) {
            return j === string.charAt(i);
        }).lenght == 1) return string.charAt(i);
    }
};
console.log(firstNonRepeating);

/*const firstNonRepeatedCharacter = function (string) {
    const chars = string.split("");
    for (let i = 0; i < string.length; i++) {
        if (chars.filter(function (j) {
            return j == string.charAt(i);
        }).length == 1) return string.charAt(i);
    }
};
console.log(firstNonRepeatedCharacter("aabbooooofffkkccjdddTTT"));*/

/* function firstNonRepeatedCharacter(string) {
    return string.split("").filter(function (character, index, obj) {
        return obj.indexOf(character) === obj.lastIndexOf(character);
    }).shift();
}

console.log(firstNonRepeatedCharacter("aabcbd")); */

function nonRepeated(str) {
    for (let i = 0; i < str.length; i++) {
        let j = str.charAt(i);
        if (str.indexOf(j) == str.lastIndexOf(j)) { /*indexOf() gets first occurrence of a character & lastIndexOf() gets the last occurrence. 
                                    So when the first occurrence is also == the last occurence, it means there's just one the character. */
            return j;
        }
    }
    return null;
}

console.log(nonRepeated("aabbooooofffkkccjdddTTT"));
