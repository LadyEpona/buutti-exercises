//Dia 22: Exercise 1: Starting out
console.log("Hello World!"); //console is and object, log() is a function

/* Terminal Commands
first command, modify with flag: start with - and something
pwd: print working director = Current location/file
ls: list = Which files are in the current file (add -al more info about files)
clear
cd: change directory = switch location (add .. goes up one directory, add - goes back to previous directory,
    add nothing takes you back to home, add path (/)buutti/js tms)
mkdir: make directory = makes new folder (mkdir new-folder)
touch: creates new file (touch new-file.txt)
cp: copy (cp new-file.txt new-folder/new-file-copy.txt)
mv: move (mc new-file-copy.txt ../new-file-copy.txt, can also rename (mv file.txt file-2.txt, and this stays
    in the same folder))
rm: remove (rm new-file-copy.txt, with folder rm -r new-folder for safety)
nano: to edit files (nano is built in editor (vim is also one)) (nano new-file.txt)
cat: concatenates files = prints out what's inside it (cat new-file.txt)
    can print multiple files (cat file-2.txt new-file.txt)
>: works with all commands, redirects = takes all of the previous commands (cat file-2.txt new-file.txt > file-3.txt)
    (printing those two files) and puts all of them in a new file
echo: prints whatever you type after it (echo Hi)
>>: adds in the end of the file (echo Hi >> file-3.txt)
man: gives documentations of the commands (man ls)
ctrl+C: exits everything
apt-get: installs packages (apt-get install filezilla)
sudo: super user do = makes you the root user (for permissions tms(maybe need with apt-get))
apt-get upgrade whatever (updates)
apt-get remove whatever
*/

// Dia 33: Exercise 2: Basic math

const a = 10;
const b = 2;

const c = a + b;
const d = a - b;
const e = a / b;
const f = a * b;
console.log(c, d, e, f);

const g = a ** b;
const h = a % b;
const i = (a + b) / 2; // vois laittaa arrayhyn ja sitten jaettuna .length
console.log(g, h, i);

// Dia 47: Exercise 1: If conditionals

let playerCount = 4 // HOX tarvii letin
if (playerCount === 4) {
    console.log("You can play this game!");
} else {
    console.log("Sorry, you can't play this game :(");
}

let isStressed = true;
let hasIcecream = true;
if (isStressed === false || hasIcecream === true) {
    console.log("Mark is happy!");
} else {
    console.log("Mark is sad :<");
}

let sunny = true;
let rainy = false;
let temperature = 20;
if (sunny === true && rainy === false && temperature >= 20) {
    console.log("It is a beach day!");
} else {
    console.log("It is not a beach day!");
}

let seesSuzy = true;
let seesDan = true;
let day = "Tuesday";
if (seesSuzy === true && seesDan !== true && day === "Tuesday"
    || seesSuzy !== true && seesDan === true && day === "Tuesday") {
    console.log("Arin is happy!");
} else {
    console.log("Arin is mad!");
}

// Dia 48: Exercise 2: Leap year

let year = 2020;
if (year % 4 === 0 && year % 100 !== 0 || year % 400 === 0) {
    console.log(`Year ${year} is a leap year`);
} else {
    console.log(`Year ${year} is not a leap year`);
}

// Dia 55: Exercise 3: Workin with strings
const str1 = "Äly";
const str2 = "puhelin";
const str_sum = str1 + str2;
console.log(str_sum);
console.log(str1.length);
console.log(str2.length);
console.log(str_sum.length);
const str_aveg = (str1.length + str2.length) / 2;
console.log(str_aveg);
if (str1.length < str_aveg) {
    console.log(str1);
} else if (str2.length < str_aveg) {
    console.log(str2);
} else if (str_sum < str_aveg) {
    console.log(str_sum);
} else {
    console.log("-");
}

// Dia 56: Exercise 4: Modify string

let string = " Super cool morning and hello world";
//trim, lowercase jaaaa??
let stringTrim = string.trim();
let stringLowCase = stringTrim.toLowerCase();
console.log(stringLowCase.substr(0, 20));

//Dia 71: Exercise 5: ESLint
const foo = 1;
console.log(foo);
const bar = 1;
const baz = 123;
function test() {
    console.log(bar);
    console.log(baz);
}
test(); //Vieläkin vähän auki... Calling the function?

// Dia 77: Exercise 1: working with arrays
const arr = ["banaani", "omena", "mandariini", "appelsiini", "kurkku", "tomaatti", "peruna"];
console.log(arr);
const filteredArr = arr.filter((arr) => {
    return arr.includes("r");
});
console.log(filteredArr);
arr.sort();
console.log(arr);
arr.shift();
console.log(arr);
arr.push("sipuli");
console.log(arr);

// Dia 82: Exercise 2: for loops

const loopsArr1 = [];
for (let ind = 0; ind < 1001; ind += 100) {
    loopsArr1.push(ind);
}
console.log(loopsArr1);

const loopsArr2 = [];
for (let i = 1; i < 129; i += i) { //lisää siis indexin itseensä; eka 1+1, 2+2 jne..
    loopsArr2.push(i);
}
console.log(loopsArr2);

const loopsArr3 = [];
for (let i = 3; i < 16; i += 3) {
    loopsArr3.push(i);
}
console.log(loopsArr3);

const loopsArr4 = [];
for (let i = 9; i > -1; i--) {
    loopsArr4.push(i);
}
console.log(loopsArr4);

const loopsArr5 = [];
for (let i = 0; i < 3; i++) {
    loopsArr5.push(i);
    loopsArr5.fill(1, 0, 3) // Hyvä tietää, että tämmönenkin olemassa
}
for (let i = 3; i < 6; i++) {
    loopsArr5.push(i);
    loopsArr5.fill(2, 3, 6) // (fill with, from position, untill)
}
for (let i = 6; i < 9; i++) {
    loopsArr5.push(i);
    loopsArr5.fill(3, 6, 9)
}
for (let i = 9; i < 12; i++) {
    loopsArr5.push(i);
    loopsArr5.fill(4, 9, 12)
}
console.log(loopsArr5); //Vau, miten kauhee :'D Mut hei! Sain tehtyä tän!

const loopsArr6 = [];
for (let i = 0; i < 5; i++) {
    loopsArr6.push(i);
}
for (let i = 0; i < 5; i++) {
    loopsArr6.push(i);
}
for (let i = 0; i < 5; i++) {
    loopsArr6.push(i);
}
console.log(loopsArr6); // Yäk

// Dia 84: Exercise 3: Sum of ints

let n = 5;
let integers = 0;
for (let ind = 0; ind <= n; ind++) {
    integers = integers + ind;
}
console.log(`The sum of integers up to this number is ${integers}`);

let n2 = 5;
let integersWhi = 0;
let ind = 0;
while (ind <= (n2)) {
    integersWhi = integersWhi + ind;
    ind = ind + 1;
}
console.log(`The sum of integers up to this number is ${integersWhi}`);

let n3 = 17;
let someIntegers = 0;
for (let ind = 0; ind <= n3; ind++)
    if (ind % 3 === 0 || ind % 5 === 0) {
        someIntegers = someIntegers + ind;
    } {
    ind = ind++;
}
console.log(`The sum of some integers up to this number is ${someIntegers}`);

// Dia 91: Exercise 5: FizzBuzz

for (let counter = 1; counter <= 100; counter++) {
    if (counter % 3 === 0 && counter % 5 === 0) {
        console.log("FizzBuzz");
    } else if (counter % 3 === 0) {
        console.log("Fizz");
    } else if (counter % 5 === 0) {
        console.log("Buzz");
    } else {
        console.log(counter);
    }
}

// Dia 92: Exercise 6: Right-angled

let numberN = 4;
let puolikas = "&";
for (let i = 1; i < numberN + 1; i++) { // i = 0 aloittais tyhjällä rivillä, +1 kuitenkin siksi että indeksi alkaa 0sta
    console.log(puolikas.repeat(i));
}
// TAI
let puolikas2 = "&";
for (let i = 0; i < numberN; i++) {
    console.log(puolikas2.repeat(i + 1)); // +1 tänne niin ei tule index 0llaan tyhjää
}

for (let i = 0; i < numberN; i++) {
    let kuusi = "&".repeat(2 * i + 1);
    let spacesBefore = " ".repeat(numberN - i - 1); //korkeus - index - default1ettätoimii ja nämä kaikki tulee vas puolelle
    console.log(spacesBefore + kuusi);
}

//While
let puoliKuusi = "&";
while (puoliKuusi.length <= numberN) {
    console.log(puoliKuusi);
    puoliKuusi = puoliKuusi + "&";
}

