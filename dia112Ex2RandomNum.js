/*Write a function that generates random number within range rounded to nearest integer. Function takes in two numbers, min and max.*/

/*function random(min, max) {
    const num = Math.floor(Math.random() * (max - min + 1)) + min;
    return num;
}

random(1, 10);*/

/* function randomNumber(min, max) {
    return Math.random() * (max - min) + min;
}
randomNumber(1, 10);
console.log(randomNumber); */
// Function to generate random number 

/*function randomNumber(min, max) {
    return Math.random() * (max - min) + min;
}
randomNumber(1, 5);*/

/* let random = Math.floor(Math.random() * 10) + 1;

console.log(random); */

function getRandomNum(min, max) {
    return Math.floor(Math.random() * ((max - min) + 1)) + min; /* +1 täytyy olla että ottaa max luvunkin, floor pyöristää alaspäin 
    eli ihan oikeasti min on 20 (inclusive) ja max 31 (exclusive)
    math.roundia ei siksi että se pyöristää ylöspäin, jolloin saattaisi printata myös luvun 31*/
}

const randomNumber = getRandomNum(20, 30);
console.log(randomNumber);