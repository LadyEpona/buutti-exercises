/* Create a program that loops through numbers from 1 to 100 and...


if the number is divisible by 3, prints “Fizz”
if the number is divisible by 5, prints “Buzz”
if the number is divisible by both (3 and 5), prints “FizzBuzz” 
 */

for (let counter = 1; counter <= 100; counter++)
    if (counter % 15 === 0) {
        console.log("FizzBuzz"); //HOX missä järjestyksessä nämä on
    } else if (counter % 5 === 0) {
        console.log("Buzz");
    } else if (counter % 3 === 0) {
        console.log("Fizz");
    } else {
        console.log(counter);
    }