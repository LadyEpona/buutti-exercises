/*Create a function, takes an number n as parameter, and produces an array of numbers, 
n being equal to the length of the array, that follows this (Fibonacci) sequence:
const arr = [1, 1, 2, 3, 5, 8, 13, 21, 34, ...]; */

function listFibonacci(n) {
    const fibonacci = [1, 1]; //arrayn alku määritelty
    let i = 0;
    for (i = 1; i <= n - 2; i++) { // -2 koska jo määritetty kaksi ekaa arvoa, muuten printtaa yht 12
        fibonacci.push(fibonacci[i] + fibonacci[i - 1]);
    }
    return fibonacci;
}
console.log(listFibonacci(10));

/* function listFibonacci(n) {
    // declare the array starting with the first 2 values of the fibonacci sequence
    // starting at array index 1, and push current index + previous index to the array
    for (var fibonacci = [1, 1], i = 1; i < n; i++)
        fibonacci.push(fibonacci[i] + fibonacci[i - 1]);

    return fibonacci;
}

console.log(listFibonacci(10)); */

/*let arr = [];
let i = 1;
arr[0] = 1;
arr[1] = 1;
for (i = 2; i <= 10; i++) {
    arr[i] = arr[i - 2] + arr[i - 1];
}
console.log(arr); */

/* var i;
var fib = []; // Initialize array!

fib[0] = 0;
fib[1] = 1;
for (i = 2; i <= 10; i++) {
    // Next fibonacci number = previous + one before previous
    // Translated to JavaScript:
    fib[i] = fib[i - 2] + fib[i - 1];
    console.log(fib[i]);
}*/