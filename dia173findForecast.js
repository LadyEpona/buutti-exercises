import fs from "fs";

const allForecasts = {
    day: "monday",
    temperature: 20,
    cloudy: true,
    sunny: false,
    windy: false,
}
/* //"vaikka tiedosto ilmestyisi listaan, voi se kirjoitusoperaatio olla vielä kesken, kun jo edetään lukuoperaatioon, 
//jos ei käytetä synkronisia metodeita" writeFile vs writeFileSync
fs.writeFile("forecast_data2.json", JSON.stringify(allForecasts), "utf8", (err) => {
    if (err) {
        console.log("Could not save forecasts to file!");
    }
}); // Mitä eroa on if(err) ja try/catch??? */

try {
    fs.writeFileSync("forecast_data2.json", JSON.stringify(allForecasts), "utf8");
} catch (e) {
    console.log("Could not save forecasts to file!");
}

try {
    const weather = fs.readFileSync("forecast_data2.json", "utf8");
    console.log(JSON.parse(weather));
} catch (e) {
    console.log("No saved data found.");
}
/* Jos haluaa tietyn valuen:
try {
    const weather = fs.readFileSync("forecast_data2.json", "utf8");
    const temp = JSON.parse(weather)
    console.log(temp.temperature);
} catch (e) {
    console.log("No saved data found.");
}*/


/* MUU Esimerkki:
let data = "This is a file containing a collection"
    + " of programming languages.\n"
    + "1. C\n2. C++\n3. Python";

fs.writeFileSync("programming.txt", data);
console.log("File written successfully\n");
console.log("The written has the following contents:");
console.log(fs.readFileSync("programming.txt", "utf8")); */