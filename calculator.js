import readline from "readline"; //Import package

//Creating the interface
const calcInterface = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

const validOperators = [
    "*",
    "-",
    "+",
    "/",
];

calcInterface.question("Enter the first number: ", (firstInput) => { //Mitä eroa on promptilla ja questionilla o.O
    const firstNumber = Number(firstInput);
    if (isNaN(firstNumber)) {
        // Deal with wrong input type here
        console.log("Invalid number provided");
        return calcInterface.close();
    }

    calcInterface.question("Enter the operator: ", (operatorInput) => { //Meni sitten vuosia ihmetellä mikä mättää kun , puuttui!
        if (!validOperators.includes(operatorInput)) {
            //Invalid operator provided
            console.log("Invalid operator provided");
            return calcInterface.close();
        }
        calcInterface.question("Enter the second number: ", (secondInput) => {
            const secondNumber = Number(secondInput);
            if (isNaN(secondNumber)) {
                // Deal with wrong input type here
                console.log("Invalid number provided");
                return calcInterface.close();
            }

            const result = calculatorLogic(firstNumber, operatorInput, secondNumber);
            console.log(`Your Result: ${result}`);
            calcInterface.close();
        });
    });
});

function calculatorLogic(firstNumber, operator, secondNumber) {
    if (operator === "+") return firstNumber + secondNumber;
    else if (operator === "-") return firstNumber - secondNumber;
    else if (operator === "*") return firstNumber * secondNumber;
    else if (operator === "/") return firstNumber / secondNumber;
}


