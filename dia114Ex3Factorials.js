/* Write a program that calculates the factorial n of a given number n

n! = n*(n-1)*(n-2)*...*1
n=4, n! = 4*3*2*1

Note: n! = factorial n 
Voi tehdä recursionilla, whileloppilla tai forloopilla*/

//1. Factorialize a Number With Recursion
/*
function factorialize(num) {
    if (num < 0) // If the number is less than 0, reject it.
        return -1;
    else if (num == 0) // If the number is 0, its factorial is 1.
        return 1;
    else { // Otherwise, call the recursive procedure again
        return (num * factorialize(num - 1));
    }
}
const total = factorialize(5);
console.log(total);*/

//2. Factorialize a Number with a WHILE loop
/*
function factorialize(num) {
    let result = num; // Step 1. Create a variable result to store num
    if (num === 0 || num === 1) // If num = 0 OR num = 1, the factorial will return 1
        return 1;
    while (num > 1) { // Step 2. Create the WHILE loop 
        num--; // decrementation by 1 at each iteration
        result *= num; // or result *= num;
    }
    return result; // Step 3. Return the factorial of the provided integer
}
const totalWhile = factorialize(5);
console.log(totalWhile); */

//3. Factorialize a Number with a FOR loop

function factorialize(num) {
    if (num === 0 || num === 1) // If num = 0 OR num = 1, the factorial will return 1
        return 1;
    for (var i = num - 1; i >= 1; i--) { // We start the FOR loop with i = 5
        // We decrement i after each iteration
        num *= i; // We store the value of num at each iteration // or num = num * i;
    }
    return num;
}
const totalFor = factorialize(5);
console.log(totalFor);
