/*Write a program that prints the sum of integers from 1 to n with a given number n

for example, if n = 5, program prints 15.

Solve this task with both a for - loop and a while - loop */

const readline = require("readline");

const rl = readline.createInterface(
    process.stdin, process.stdout);

rl.setPrompt("Give a number, please: ");
rl.prompt();
rl.on("line", (number) => {
    let integers = 0;
    let ind = 0;
    while (ind <= (number)) {
        integers = integers + ind;
        ind = ind + 1;
    }
    console.log(`The sum of integers up to this number is ${integers}`);
    rl.close();

});
