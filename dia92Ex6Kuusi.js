/* Create a program that takes in a number n and prints out a triangle of &’s with the height of n

Level 1: if n = 4, program prints 
&
&&
&&&
&&&& 

REPEAT String

Level 2:							
    &
   &&&
  &&&&&
 &&&&&&&

 if you solved this using for - loops, now solve it using while loops
*/

let puolikas = "&";
for (let i = 1; i < 5; i++) {
    console.log(puolikas.repeat(i));
}

for (let i = 0; i < 4; i++) {
    // 2n+1 stars per row i.e. 1, 3, 5, 7, ...
    let stars = "&".repeat(2 * i + 1);
    let spacesBefore = " ".repeat(4 - i - 1); //korkeus - index - default1ettätoimii ja nämä kaikki tulee vas puolelle
    console.log(spacesBefore + stars);
}
// Ylemmät forLoopilla ok, whileLoop vielä vaiheessa... //
/*
let ind = 0;
let sum = 0;

while (ind < 4) {
    let stars = "&".repeat(2 * ind + 1);
    let spacesBefore = " ".repeat(4 - ind - 1);
}
console.log(spacesBefore + stars); */

let wpuolikas = 0;
let i = 0;
while (i < 5) {
    wpuolikas = "&" + i;
    i = i + 1;
}
console.log(wpuolikas.repeat(i));
